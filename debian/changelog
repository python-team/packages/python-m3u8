python-m3u8 (6.0.0-2) unstable; urgency=medium

  * Team Upload
  * Fix syntax error in d/control that confuses UDD & the Tracker

 -- Alexandre Detiste <tchet@debian.org>  Mon, 23 Dec 2024 16:56:02 +0100

python-m3u8 (6.0.0-1) unstable; urgency=medium

  * Team Upload
  * New upstream version 6.0.0
  * Use dh-sequence-python3

  [ Ondřej Nový ]
  * Remove myself from Uploaders.

 -- Alexandre Detiste <tchet@debian.org>  Sat, 30 Nov 2024 01:35:45 +0100

python-m3u8 (0.8.0-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 08 Dec 2022 05:00:47 +0000

python-m3u8 (0.8.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on dh-python.

  [ Sandro Tosi ]
  * debian/control
    - use proper team name and email address

 -- Sandro Tosi <morph@debian.org>  Tue, 31 Aug 2021 00:43:19 -0400

python-m3u8 (0.8.0-1) unstable; urgency=medium

  * New upstream release.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.
  * d/watch: Bump version.
  * d/CHANGELOG: Update from new upstream release.
  * Bump Standards-Version to 4.5.1.

 -- Ondřej Nový <onovy@debian.org>  Mon, 04 Jan 2021 12:25:51 +0100

python-m3u8 (0.7.1-1) unstable; urgency=medium

  * New upstream release

 -- Ondřej Kobližek <kobla@debian.org>  Wed, 26 Aug 2020 09:29:35 +0200

python-m3u8 (0.6.0-1) unstable; urgency=medium

  * New upstream release
  * d/control:
    - Bump debhelper version to 13
    - Update mailing-list address in Maintainer

 -- Ondřej Kobližek <kobla@debian.org>  Fri, 15 May 2020 20:22:57 +0200

python-m3u8 (0.5.4-1) unstable; urgency=medium

  * Bump Standards-Version to 4.5.0.
  * New upstream release

 -- Ondřej Kobližek <kobla@debian.org>  Wed, 22 Jan 2020 07:29:24 +0100

python-m3u8 (0.5.3-1) unstable; urgency=medium

  * New upstream release

 -- Ondřej Kobližek <kobla@debian.org>  Thu, 19 Dec 2019 08:09:15 +0100

python-m3u8 (0.5.2-1) unstable; urgency=medium

  * New upstream release

 -- Ondřej Kobližek <kobla@debian.org>  Tue, 19 Nov 2019 07:15:01 +0100

python-m3u8 (0.5.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Ondřej Kobližek ]
  * New upstream release
  * Change my email address

 -- Ondřej Kobližek <kobla@debian.org>  Thu, 07 Nov 2019 17:01:03 +0100

python-m3u8 (0.5.0-1) unstable; urgency=medium

  * New upstream release
  * Regenerate upstream changelog
  * d/control: Add Rules-Requires-Root

 -- Ondřej Kobližek <koblizeko@gmail.com>  Wed, 02 Oct 2019 12:01:01 +0200

python-m3u8 (0.4.0-1) unstable; urgency=medium

  * New upstream release
  * Regenerate upstream changelog

 -- Ondřej Kobližek <koblizeko@gmail.com>  Mon, 16 Sep 2019 09:22:25 +0200

python-m3u8 (0.3.12-1) unstable; urgency=medium

  * New upstream release
  * Regenerate upstream changelog

 -- Ondřej Kobližek <koblizeko@gmail.com>  Mon, 26 Aug 2019 07:39:00 +0200

python-m3u8 (0.3.11-1) unstable; urgency=medium

  * New upstream release
  * Regenerate upstream changelog

 -- Ondřej Kobližek <koblizeko@gmail.com>  Mon, 05 Aug 2019 07:16:26 +0200

python-m3u8 (0.3.10-2) unstable; urgency=medium

  * Change of distribution from experimental to unstable
  * d/control: Drop the Python 2 module
  * d/rules: Drop the Python 2 module
  * d/tests: Drop the Python 2 module test
  * Bump standards version to 4.4.0 (no changes)

 -- Ondřej Kobližek <koblizeko@gmail.com>  Thu, 11 Jul 2019 10:03:46 +0200

python-m3u8 (0.3.10-1) experimental; urgency=medium

  * New upstream release
  * Regenerate upstream changelog

 -- Ondřej Kobližek <koblizeko@gmail.com>  Thu, 27 Jun 2019 09:09:23 +0200

python-m3u8 (0.3.9-1) experimental; urgency=medium

  * New upstream release
  * Regenerate upstream changelog

 -- Ondřej Kobližek <koblizeko@gmail.com>  Wed, 05 Jun 2019 13:00:28 +0200

python-m3u8 (0.3.8-1) experimental; urgency=medium

  * New upstream release
  * d/control: Remove unnecessary dependency python(3)-arrow
  * Regenerate upstream changelog

 -- Ondřej Kobližek <koblizeko@gmail.com>  Tue, 09 Apr 2019 08:30:22 +0200

python-m3u8 (0.3.7-1) unstable; urgency=medium

  * New upstream release
  * Bump standards version to 4.3.0 (no changes)
  * Bump debhelper compat level to 12 and use debhelper-compat
  * Rename d/tests/control.autodep8 to debian/tests/control

 -- Ondřej Nový <onovy@debian.org>  Mon, 31 Dec 2018 22:26:49 +0100

python-m3u8 (0.3.5-2) unstable; urgency=medium

  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Bump debhelper compat level to 11
  * Bump standards version to 4.2.1 (no changes)
  * Add upstream metadata

 -- Ondřej Nový <onovy@debian.org>  Sat, 29 Sep 2018 20:48:59 +0200

python-m3u8 (0.3.5-1) unstable; urgency=medium

  * New upstream release

 -- Ondřej Kobližek <koblizeko@gmail.com>  Tue, 14 Nov 2017 13:14:21 +0100

python-m3u8 (0.3.4-1) unstable; urgency=medium

  * New upstream release
  * Standards version is 4.1.1 now (no changes needed)

 -- Ondřej Kobližek <koblizeko@gmail.com>  Wed, 01 Nov 2017 08:12:27 +0100

python-m3u8 (0.3.3-1) unstable; urgency=medium

  * New upstream release
  * Standards version is 4.1.0 now (no changes needed)

 -- Ondřej Nový <onovy@debian.org>  Tue, 26 Sep 2017 16:17:57 +0200

python-m3u8 (0.3.2-2) unstable; urgency=medium

  * Https form of the copyright-format URL
  * Standards version is 4.0.0 now (no changes needed)
  * Change of distribution from experimental to unstable

 -- Ondřej Kobližek <koblizeko@gmail.com>  Fri, 23 Jun 2017 13:13:01 +0200

python-m3u8 (0.3.2-1) experimental; urgency=medium

  * New upstream release
  * Add autopkgtest-pkg-python testsuite

 -- Ondřej Kobližek <koblizeko@gmail.com>  Mon, 24 Apr 2017 14:13:57 +0200

python-m3u8 (0.3.1-1) unstable; urgency=medium

  * New upstream release
  * Bumped debhelper version to 10

 -- Ondřej Nový <onovy@debian.org>  Tue, 24 Jan 2017 10:40:32 +0100

python-m3u8 (0.3.0-1) unstable; urgency=medium

  * New upstream release

 -- Ondřej Nový <onovy@debian.org>  Fri, 07 Oct 2016 22:25:39 +0200

python-m3u8 (0.2.10-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump required version of dh-python to 2.20160609~
  * d/rules: Use pybuild for running tests

  [ Ondřej Kobližek ]
  * New upstream release

 -- Ondřej Kobližek <koblizeko@gmail.com>  Fri, 12 Aug 2016 13:10:12 +0200

python-m3u8 (0.2.9-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Removed version from python-all build dependency
  * d/{control,copyright}: Use my @debian.org email address

  [ Ondřej Kobližek ]
  * New upstream release
  * Standards version is 3.9.8 now (no changes needed)
  * Added myself as uploader
  * d/watch: added python prefix for tarball
  * Added myself to copyright file for Debian part

 -- Ondřej Kobližek <koblizeko@gmail.com>  Thu, 07 Jul 2016 12:15:16 +0200

python-m3u8 (0.2.8-1) unstable; urgency=medium

  * Initial release. (Closes: #814983)

 -- Ondřej Nový <novy@ondrej.org>  Sun, 28 Feb 2016 21:11:04 +0100
